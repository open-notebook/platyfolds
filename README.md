Lagrangian platyfolds
=====================

This is an open research notebook.

If you have any thoughts or comments, I'd be very happy to hear from
you (either via email, via bug reports or via a git pull request).

The remit of this particular notebook is the topic of Lagrangian
submanifolds which are free quotients of tori.

Contents of notebook
--------------------

  * platyfolds.lzl (the source code for platyfolds.pdf)
